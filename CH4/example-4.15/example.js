/**
 * Created by yangyanjun on 15/6/5.
 */

var canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    video = document.getElementById('video');

// Functions...

function animate() {
    if (!video.ended) {
        context.drawImage(video, 0, 0, canvas.width, canvas.height);
        window.requestAnimationFrame(animate);
    }
}

// Event handlers...

video.onload = function (e) {
    video.play();
    window.requestAnimationFrame(animate);
};

alert('This example plays a video, but due to copyright restrictions and size limitations, the video is not included in the code for this example. To make this example work, download a video, and replace the two source elements in example.html to refer to your video.');
