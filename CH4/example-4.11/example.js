/**
 * Created by yangyanjun on 15/6/5.
 */

var image = new Image(),
    canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    sunglassButton = document.getElementById('sunglassButton'),
    sunglassOn = false,
    sunglassFilter = new Worker('sunglassFilter.js');

// Functions...

function putSunglassOn() {
    sunglassFilter.postMessage(
        context.getImageData(0, 0, canvas.width, canvas.height));

    sunglassFilter.onmessage = function (event) {
        context.putImageData(event.data, 0, 0);
    };
}

function drawOriginalImage() {
    context.drawImage(image, 0, 0,
        image.width, image.height, 0, 0,
        canvas.width, canvas.height);
}

// Event handlers

sunglassButton.onclick = function() {
    if (sunglassOn) {
        sunglassButton.value = 'Sunglasses';
        drawOriginalImage();
        sunglassOn = false;
    }
    else {
        sunglassButton.value = 'Original picture';
        putSunglassOn();
        sunglassOn = true;
    }
};

// Initializations...

image.src = '../../shared/images/curved-road.png';
image.onload = function() {
    drawOriginalImage();
};