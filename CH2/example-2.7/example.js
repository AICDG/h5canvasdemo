/**
 * Created by yangyanjun on 15/5/25.
 */

var canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    SHADOW_COLOR = 'rgba(0,0,0,0.7)';


context.strokeStyle = 'cornflowerblue' ;
context.fillStyle = 'rgba(0,0,255,0.5)';
context.shadowColor = SHADOW_COLOR;
context.shadowOffsetX = 4;
context.shadowOffsetY = 5;
context.shadowBlur = 5;

context.strokeRect(75, 100, 200, 200);
context.fillRect(325, 100, 200, 200);
