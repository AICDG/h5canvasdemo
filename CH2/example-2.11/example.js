/**
 * Created by yangyanjun on 15/5/29.
 */

var context = document.getElementById('canvas').getContext('2d');

context.lineWidth = 1;
context.beginPath();
context.moveTo(50, 10);
context.lineTo(450, 10);
context.stroke();

context.beginPath();
context.moveTo(50.5, 50.5);
context.lineTo(450.5, 50.5);
context.stroke();

context.lineWidth = 2;
context.beginPath();
context.moveTo(49, 91);
context.lineTo(449, 91);
context.stroke();