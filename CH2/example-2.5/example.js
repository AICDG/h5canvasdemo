/**
 * Created by yangyanjun on 15/5/25.
 */

var canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),
    /*
     createRadialGradient(x1,y1,r1,x2,y2,r2);
     在(x1,y1)出绘制半径r1的圆
     在(x2,y2)出绘制半径r2的圆
     在两个圆组成的扇形中绘制颜色
     */
    gradient = context.createRadialGradient(canvas.width/2, canvas.height, 10,
        canvas.width/2, 0, 100);

gradient.addColorStop(0,    'blue');
gradient.addColorStop(0.25, 'white');
gradient.addColorStop(0.5,  'purple');
gradient.addColorStop(0.75, 'red');
gradient.addColorStop(1,    'yellow');

context.fillStyle = gradient;
context.rect(0, 0, canvas.width, canvas.height);
context.fill();