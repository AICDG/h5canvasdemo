/**
 * Created by yangyanjun on 15/6/3.
 */

var canvas = document.getElementById('canvas'),
    context = canvas.getContext('2d'),

    fontSelect = document.getElementById('fontSelect'),
    sizeSelect = document.getElementById('sizeSelect'),
    strokeStyleSelect = document.getElementById('strokeStyleSelect'),
    fillStyleSelect = document.getElementById('fillStyleSelect'),

    GRID_STROKE_STYLE = 'lightgray',
    GRID_HORIZONTAL_SPACING = 10,
    GRID_VERTICAL_SPACING = 10,

    cursor = new TextCursor(),

    line,

    blinkingInterval,
    BLINK_TIME = 1000,
    BLINK_OFF = 300;

// General-purpose functions...

function drawBackground() { // Ruled paper
    var STEP_Y = 12,
        i = context.canvas.height;

    context.strokeStyle = 'rgba(0,0,200,0.225)';
    context.lineWidth = 0.5;

    context.save();

    while(i > STEP_Y*4) {
        context.beginPath();
        context.moveTo(0, i);
        context.lineTo(context.canvas.width, i);
        context.stroke();
        i -= STEP_Y;
    }

    context.strokeStyle = 'rgba(100,0,0,0.3)';
    context.lineWidth = 1;

    context.beginPath();

    context.moveTo(35,0);
    context.lineTo(35,context.canvas.height);
    context.stroke();

    context.restore();
}

function windowToCanvas(x, y) {
    var bbox = canvas.getBoundingClientRect();
    return { x: x - bbox.left * (canvas.width  / bbox.width),
        y: y - bbox.top  * (canvas.height / bbox.height)
    };
}

// Drawing surface...

function saveDrawingSurface() {
    drawingSurfaceImageData = context.getImageData(0, 0,
        canvas.width,
        canvas.height);
}

// Text...

function setFont() {
    context.font = sizeSelect.value + 'px ' + fontSelect.value;
}

function blinkCursor(x, y) {
    clearInterval(blinkingInterval);
    blinkingInterval = setInterval( function (e) {
        cursor.erase(context, drawingSurfaceImageData);

        setTimeout( function (e) {
            if (cursor.left == x &&
                cursor.top + cursor.getHeight(context) == y) {
                cursor.draw(context, x, y);
            }
        }, 300);
    }, 1000);
}

function moveCursor(x, y) {
    cursor.erase(context, drawingSurfaceImageData);
    saveDrawingSurface();
    context.putImageData(drawingSurfaceImageData, 0, 0);

    cursor.draw(context, x, y);
    blinkCursor(x, y);
}

// Event handlers...

canvas.onmousedown = function (e) {
    var loc = windowToCanvas(e.clientX, e.clientY),
        fontHeight = context.measureText('W').width;

    fontHeight += fontHeight/6;
    line = new TextLine(loc.x, loc.y);
    moveCursor(loc.x, loc.y);
};

fillStyleSelect.onchange = function (e) {
    cursor.fillStyle = fillStyleSelect.value;
    context.fillStyle = fillStyleSelect.value;
};

strokeStyleSelect.onchange = function (e) {
    cursor.strokeStyle = strokeStyleSelect.value;
    context.strokeStyle = strokeStyleSelect.value;
};

// Initialization...

fontSelect.onchange = setFont;
sizeSelect.onchange = setFont;

cursor.fillStyle = fillStyleSelect.value;
cursor.strokeStyle = strokeStyleSelect.value;

context.fillStyle = fillStyleSelect.value;
context.strokeStyle = strokeStyleSelect.value;

context.lineWidth = 2.0;

setFont();
drawBackground();
saveDrawingSurface();